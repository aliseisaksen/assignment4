package assign.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.Project;

/* ===============================================================
 * 		Service Layer: calls eavesdrop, parses the result
 * ===============================================================
 */

public class EavesdropProjectsService {

	
		public EavesdropProjectsService() {

		}
		
		
		public List<Project> getEavesdropProjects() {
					
			String meetingsURL = "http://eavesdrop.openstack.org/meetings/";
			String irclogsURL = "http://eavesdrop.openstack.org/irclogs/";

			List<Project> projects = getProjectsList(meetingsURL);
			projects.addAll(getProjectsList(irclogsURL));

			return projects;
		}

		
		public static List<Project> getProjectsList(String urlString) {
			
			List<Project> retVal = new ArrayList<Project>();
			
			try {	
				
				URL url = new URL(urlString);
				URLConnection connection = url.openConnection();
				
				// Read in all HTML into readData
				String readData = readDataFromEavesdrop(connection);
				
				// Move all body anchors (AKA project names) into list
				List<String> projectNames = parseOutput(readData, true);
				
				//For each project name, create a Project and add it to Projects
				for(int i = 0; i < projectNames.size(); i++) {
				
					Project project = new Project();
					project.setName(projectNames.get(i));
					retVal.add(project);	
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			return retVal;
		}
		
		
		public static List<String> getLinksList(String urlString) {
			
			List<String> retVal = new ArrayList<String>();
			
			try {	
				
				URL url = new URL(urlString);
				URLConnection connection = url.openConnection();
				
				// Read in all HTML into readData
				String readData = readDataFromEavesdrop(connection);
				
				// Move all body anchors (AKA link names) into list
				List<String> links = parseOutput(readData, false);
				
				retVal = links;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			return retVal;
		}
		
		
		public List<String> getProjectLinks(String projectString) {
			
			projectString = projectString.replace("#", "%23");
			String projectURL = "http://eavesdrop.openstack.org/irclogs/" + projectString + "/";

			List<String> links = getLinksList(projectURL);

			return links;
		}
		
		
		public static String readDataFromEavesdrop(URLConnection connection) {
			
			String retVal = "";
			try {
				BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					retVal += inputLine;
				}
				in.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		
		// Insert all links from HTML input parameter into a List<>
		public static List<String> parseOutput(String inputString, Boolean isDirectory) {

			// Gather all links
			List<String> linkNames = new ArrayList<String>();
			Document doc = Jsoup.parse(inputString);
		    Elements links = doc.select("a");
		    
		    ListIterator<Element> iter = links.listIterator();
		    
		    // Skip header links (Name, Last Modified, Size, Description, Parent directory)
		    for(int i = 0; i < 5; i++)
		    {
		    	iter.next();
		    }
		    
		    // Insert link titles into
		    while(iter.hasNext()) {
	    		Element e = (Element) iter.next();
	    		String s = e.html();
	    		//s = s.replace("#", "%23");
	    		if(isDirectory)
	    			s = s.substring(0, s.length()-1); // deletes ending '/'
	    		linkNames.add(s);
		    }

			return linkNames;
		}
		

}