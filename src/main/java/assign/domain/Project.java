package assign.domain;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	
	@XmlElement
	private String name;
	
	@XmlElement
	private ArrayList<String> link = new ArrayList<String>();

	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<String> getLinks() {
		return link;
	}
	
	public void setLink(String link) {
		this.link.add(link);
	}

}