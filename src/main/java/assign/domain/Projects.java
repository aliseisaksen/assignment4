package assign.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "projects")
@XmlAccessorType
public class Projects {

    private List<Project> projects = new ArrayList<Project>();
 
    public List<Project> getProjects() {
        return projects;
    }

    @XmlElement(name = "project")
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }	
}