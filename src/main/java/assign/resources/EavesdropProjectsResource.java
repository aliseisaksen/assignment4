package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Project;
import assign.domain.Projects;
import assign.services.EavesdropProjectsService;

import javax.servlet.http.HttpServletResponse;

/* ===============================================================
 * 		Resource Layer: input validation, response generation
 * ===============================================================
 */

@Path("/myeavesdrop")
public class EavesdropProjectsResource {
	
	private EavesdropProjectsService eavesdropProjectsService;
	
	public EavesdropProjectsResource() {
		this.eavesdropProjectsService = new EavesdropProjectsService();
	}
	
	
	/* ===============================================================
	 * 					Return union of all projects
	 * ===============================================================
	 */
	@GET
	@Path("/projects")
	@Produces("application/xml")
	public StreamingOutput getAllProjects() throws Exception {
	
		final Projects projects = new Projects();	
		projects.setProjects(eavesdropProjectsService.getEavesdropProjects());
		    
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProjects(outputStream, projects);
	         }
	      };	    
	}
	
	
	/* ===============================================================
	 * 					Return irclogs resource projects
	 * ===============================================================
	 */
	@GET
	@Path("/projects/{project}/irclogs")
	@Produces("application/xml")
	public StreamingOutput getIrclogProject(@PathParam("project") String projectIn, @Context final HttpServletResponse response) throws Exception {
	
		final Project project = new Project();	
		
		// Validate input project name
		Boolean validProject = false;
		validProject = verifyProjectIn(projectIn);
		
		if(validProject) {
			// Build project based on projectIn name and links found in response body
			projectIn = projectIn.replace("#", "%23");
			project.setName(projectIn);
			List<String> links = eavesdropProjectsService.getProjectLinks(projectIn);
			for(int i = 0; i < links.size(); i++) {
				project.setLink("http://eavesdrop.openstack.org/irclogs/" + projectIn + "/" + links.get(i));
			}
			
			return new StreamingOutput() {
		         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
		            outputProject(outputStream, project);
		         }
		      };
			
		} else
			// If project is invalid, respond with 404 Not Found
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		
	}
	
	
	/* ===============================================================
	 * 						Input Validation
	 * ===============================================================
	 */
	public Boolean verifyProjectIn(String project) {
		
		Boolean validProject = false;
		String urlString = "http://eavesdrop.openstack.org/irclogs/";
		List<Project> projectList = EavesdropProjectsService.getProjectsList(urlString);
		
		// Check if String parameter matches a project name
		for(int i = 0; i < projectList.size(); i++) {
			if(projectList.get(i).getName().equals(project))
				validProject = true;
		}
		return validProject;
	}
	
	
	/* ===============================================================
	 * 						Output Projects
	 * ===============================================================
	 */
	protected void outputProjects(OutputStream os, Projects projects) throws IOException {
		
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(projects, os);
		} catch (JAXBException jaxb) {
			throw new WebApplicationException();
		}
	}
	
	
	/* ===============================================================
	 * 						Output Project
	 * ===============================================================
	 */
	protected void outputProject(OutputStream os, Project project) throws IOException {
		
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
}