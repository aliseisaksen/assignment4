package assign.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.junit.Test;

/* ===============================================================
 * 	Implement functional tests for the following three resources:
 * 
 * 	1) GET http://localhost:8080/assignment4/myeavesdrop/projects/
 * 
 * 	2) GET http://localhost:8080/assignment4/myeavesdrop/projects/%23openstack-api/irclogs
 * 
 * 	3) GET http://localhost:8080/assignment4/myeavesdrop/projects/non-existent-project/irclogs
 * ===============================================================
 */

public class EavesdropProjectsResourceTest {

	@Test
	public void testEavesdropProjectsResource() throws Exception {
	
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects")
		                 .request().get();
			
			String projects = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(projects);
	        for (Element e : doc.getElementsByTag("name")) {
	             
	        	String s = e.html();
	       	 	xmlContent.add(s);
	        	 
	         }
	     
	        // Run assertions
	        // Test for some /meetings projects
	        if (!xmlContent.contains("3rd_party_ci"))
	        	throw new RuntimeException("Missing project: 3rd_party_ci");
	        if (!xmlContent.contains("neutron"))
	        	throw new RuntimeException("Missing project: neutron");
	        if (!xmlContent.contains("zaqar"))
	        	throw new RuntimeException("Missing project: zaqar");
	         
	        // Test for some /irclogs projects
	        if (!xmlContent.contains("#dox"))
	        	throw new RuntimeException("Missing project: #dox"); 
	        if (!xmlContent.contains("#openstack-oslo"))
	        	throw new RuntimeException("Missing project: #openstack-oslo");
	        if (!xmlContent.contains("#tripleo"))
	        	throw new RuntimeException("Missing project: #tripleo");

	        response.close();
	        
		} finally {
	        client.close();
	    }
		
	}
	
	@Test
	public void testEavesdropIrclogsResourceCorrectInput() throws Exception {
		
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/%23openstack-api/irclogs")
					.request().get();
			
			String links = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(links, "", Parser.xmlParser());
	        for (Element e : doc.select("link")) {
	             
	        	String s = e.html();
	       	 	xmlContent.add(s);
	       	 	
	         }
	        
	        for (Element e : doc.select("name")) {
	             
	        	String s = e.html();
	       	 	xmlContent.add(s);
	       	 	
	         }
	        
	        System.out.println(xmlContent.toString());
	        
	        // Run assertions
	        // Test that project name is displayed
	        if (!xmlContent.contains("%23openstack-api"))
	        	throw new RuntimeException("Missing XML element: project name #openstack-api");
	        
	        // Test for some .log link entries
	        if (!xmlContent.contains("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-05.log"))
	        	throw new RuntimeException("Missing document: #openstack-api.2015-03-05.log");
	        if (!xmlContent.contains("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-20.log"))
	        	throw new RuntimeException("Missing document: #openstack-api.2015-03-20.log");
	        if (!xmlContent.contains("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-04-05.log"))
	        	throw new RuntimeException("Missing document: #openstack-api.2015-04-05.log");

	        response.close();
	        
		} finally {
	        client.close();
	    }
	}
	
	@Test
	public void testEavesdropIrclogsResourceIncorrectInput() throws Exception {
		
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/non-existent-project/irclogs")
					.request().get();
	        
	        // Run assertions
	        if (response.getStatus() != 404) throw new RuntimeException("404 Not Found response not generated");
	        response.close();
	        
		} finally {
	        client.close();
	    }
		
	}
	
}
